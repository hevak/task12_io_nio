package com.epam.edu.online.readjavasource;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class CommentReader {
    public static void printComment(String path) {
        try (BufferedReader bis =
                     new BufferedReader(new InputStreamReader(new FileInputStream(path), StandardCharsets.UTF_8))) {
            int read = bis.read();
            while (read != -1) {
                read = bis.read();
                if ((char) read == '/') {
                    read = bis.read();
                    if (((char) read) == '/') {
                        System.out.println(bis.readLine());
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
