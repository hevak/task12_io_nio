package com.epam.edu.online.clientserver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class Client {
    private final static Logger log = LogManager.getLogger(Client.class);

    private static final int BUFFER_SIZE = 1024;
    private static final int port = 9999;
    private static String[] messages =
            {"The best way to predict the future is to create it.",
                    "As you think, so shall you become.",
                    "The noblest pleasure is the joy of understanding.",
                    "Courage is grace under pressure.",
                    "*exit*"};

    public static void main(String[] args) {

        log.info("Starting MySelectorClientExample...");
        try {
            InetAddress hostIP = InetAddress.getLocalHost();
            InetSocketAddress myAddress =
                    new InetSocketAddress(hostIP, port);
            SocketChannel myClient = SocketChannel.open(myAddress);

            log.info(String.format("Trying to connect to %s:%d...",
                    myAddress.getHostName(), myAddress.getPort()));

            for (String msg: messages) {
                ByteBuffer myBuffer=ByteBuffer.allocate(BUFFER_SIZE);
                myBuffer.put(msg.getBytes());
                myBuffer.flip();
                int bytesWritten = myClient.write(myBuffer);
                log.info(String
                        .format("Sending Message...: %s\nbytesWritten...: %d",
                                msg, bytesWritten));
            }
            log.info("Closing Client connection...");
            myClient.close();
        } catch (IOException e) {
            log.info(e.getMessage());
            e.printStackTrace();
        }
    }
}
