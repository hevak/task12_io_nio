package com.epam.edu.online.serialization;

import com.epam.edu.online.serialization.model.Droid;
import com.epam.edu.online.serialization.model.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class Application {
    private final static Logger log = LogManager.getLogger(Application.class);
    private static final String path = "serializationFiles" + File.separator + "ship.ser";

    public static void main(String[] args) {
        Ship<Droid> ship = new Ship("new Ship");
        ship.getDroid().forEach(log::info);

        // save data into file
        SerializeDeserialize.serialize(ship, path);

        // read data from file
        Ship deserializeShip = SerializeDeserialize.deserialize(path);
        log.info("Object has been deserialized");
        deserializeShip.getDroid().forEach(log::info);
    }
}
