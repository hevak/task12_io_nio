package com.epam.edu.online.serialization.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Ship<T extends Serializable> implements Serializable {

    private String name;
    private List<T> droid;

    public Ship(String name) {
        this.name = name;
        this.droid = (List<T>) fillDroids();
    }


    public List<T> getDroid() {
        return droid;
    }

    private static List<Droid> fillDroids() {
        return new ArrayList<>(Arrays.asList(
                new Droid(1, "droid-1", 1.0, 99),
                new Droid(2, "droid-2", 1.1, 99),
                new Droid(3, "droid-3", 2.0, 99)
        ));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ship<?> ship = (Ship<?>) o;

        if (name != null ? !name.equals(ship.name) : ship.name != null) return false;
        return droid != null ? droid.equals(ship.droid) : ship.droid == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (droid != null ? droid.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "name='" + name + '\'' +
                ", droid=" + droid +
                '}';
    }
}
