package com.epam.edu.online.serialization.model;

import java.io.Serializable;

public class Droid implements Serializable {

    private int id;
    private String name;
    private Double version;
    private transient int transientField;

    public Droid(int id, String name, Double version, int transientField) {
        this.id = id;
        this.name = name;
        this.version = version;
        this.transientField = transientField;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getVersion() {
        return version;
    }

    public int getTransientField() {
        return transientField;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", version=" + version +
                ", transientField=" + transientField +
                '}';
    }
}
